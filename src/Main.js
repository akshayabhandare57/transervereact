import React, { Component } from "react";
import { Container, Row, Col } from 'react-grid-system';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import MenuItem from "@material-ui/core/MenuItem";
import SideBar from './Sidebar';
import Header from './Header';
import './Sidebar.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    height: 400,
    overflowX:'scroll',
  },
  selectedRow: {
    backgroundColor: '#4f63843d!important',
    ':hover': {
      backgroundColor: '#587cb7',
    },
  },
});
 
class Main extends Component {
    
  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;
    return (
      <div>
        {/* Header */}
        <Header />
      {/* Sidebar */}
      <Row style={{backgroundColor: "white", color: "black", padding: "10px", margin: "5px"}}>
        <Col sm={1}>
          <SideBar />
        </Col>
        <Col sm={11}>
          <Row sm={8}>
            <Col><h2>Dashboard</h2></Col>
            <Col sm={2}>
              <FormControl
                fullWidth
                variant="outlined"
              >
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  required={true}
                  htmlFor="Projects"
                >
                  Projects:
                </InputLabel>
                <Select
                  value={"this.state.projects"}
                  input={
                    <OutlinedInput
                      name="projects"
                      labelWidth={80}
                      labelHeight={10}
                      id="projects"
                    />
                  }
                >
                  {[
                    { text: "Project 1", value: "1" },
                    { text: "Project 2", value: "2" }
                  ].map((projects, idx) => (
                    <MenuItem key={idx} value={projects.value}>
                      {projects.text}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Col>
            <Col sm={2}>
            <FormControl
                fullWidth
                variant="outlined"
              >
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  required={true}
                  htmlFor="Employees"
                >
                  Employees
                </InputLabel>
                <Select
                  value={"this.state.employees"}
                  input={
                    <OutlinedInput
                      name="employees"
                      labelWidth={80}
                      labelHeight={10}
                      id="employees"
                    />
                  }
                >
                  {[
                    { text: "Employee 1", value: "1" },
                    { text: "Employee 2", value: "2" }
                  ].map((employees, idx) => (
                    <MenuItem key={idx} value={employees.value}>
                      {employees.text}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Col>
          </Row> 
          <hr></hr>   
          <Row style={{padding: "20px"}}>
            <Col sm={12}> Project Status</Col>
          </Row> 
          <Row>
            <Col sm={12} > 
                    <div style={{backgroundColor: "grey",padding: "60px", textAlign: "center"}}>
                      <Row style={{padding: "10px"}}>
                        <Col sm={6}>Project Time Log: </Col>
                        <Col sm={6}> 35 Hrs</Col>
                      </Row>
                      <Row style={{padding: "10px"}}>
                        <Col sm={6}>Project Estimated Time: </Col>
                        <Col sm={6}>60 Hrs</Col>
                      </Row>
                      <Row style={{padding: "10px"}}> 
                        <Col sm={6}>All Employee Time: </Col>
                        <Col sm={6}> 124 Hrs</Col>
                      </Row>
                    </div>
            </Col>
          </Row>        
            <hr></hr>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>Project</TableCell>
                  <TableCell>Employee</TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Logged Time</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>1</TableCell>
                  <TableCell>1</TableCell>
                  <TableCell>wdd</TableCell>
                  <TableCell>22-09-2020</TableCell>
                  <TableCell>3 hrs</TableCell>
                </TableRow>
              </TableBody>
              <TableFooter>
            <TableRow className={'footer'}>
              <TablePagination
                className={classes.thColor}
                count={this.props.count}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </TableRow>
          </TableFooter>
            </Table>
        </Col>
      </Row>
    </div>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};
 
export default withStyles(styles)(withRouter(Main));
//export default Main;