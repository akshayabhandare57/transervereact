import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';

class PositionedSnackbar extends React.Component {
  state = {
    open: true,
    vertical: 'top',
    horizontal: 'center',
  };

  handleClick = state => () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {

    console.log('Called...');

    const { open } = this.state;
    return (
      <div>
        <Button onClick={this.handleClick()}>
          Bottom-Right
        </Button>
        <Snackbar
          anchorOrigin={{ vertical:'bottom', horizontal:'right' }}
          open={open}
          autoHideDuration={500}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">I love snacks</span>}
        />
      </div>
    );
  }
}


const triggerSuccessNotification = () => {
  console.log('------------');
  return <Grid><PositionedSnackbar /></Grid>
};

export default triggerSuccessNotification;