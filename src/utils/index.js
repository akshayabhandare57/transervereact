import {
  Cookies
} from 'react-cookie';

//import { request } from 'http';
import {
  rejects
} from 'assert';
import {
  resolve
} from 'path';
import request from 'request';

let cookies = new Cookies();
const MAX_AGE = 31536000 // a year

class AppConfig {
  constructor() {
    this.config = this.getconfig();
    this.value = null;
  }

  fetchConfig() {
    return new Promise(function (resolve, reject) {
      request.get(window.location.origin + '/app.config.json', function (err, res, body) {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(body));
        }
      });
    }).then(function (value) {
      return value;
    });
  }

  getconfig() {
    return this.fetchConfig();
  }
}

let instance = new AppConfig();
export default instance;