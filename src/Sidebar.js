import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import * as ROUTES from './routes';

export default props => {
  return (
    <Menu style ={{position: "fixed"}}>
      <a className="menu-item" id="dashboard" href={ROUTES.getRoutePath(ROUTES.MAIN)}>
        Dashboard
      </a>
      <a className="menu-item" id="employees" href={ROUTES.getRoutePath(ROUTES.EMPLOYEES)}>
        Employees
      </a>
      <a className="menu-item" id="projects" href={ROUTES.getRoutePath(ROUTES.PROJECTS)}>
        Projects
      </a>
    </Menu>
  );
};