export const MAIN = '/Main';
export const EMPLOYEES = '/Employees';
export const PROJECTS = '/Projects';
export const LOGTIME = '/LogTime';
export const getRoutePath = (path) => {
    return window.location.href.replace(/#(.*)$/, '') + '#' + path;
  };