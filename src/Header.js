import React from 'react';
import { Container, Row, Col } from 'react-grid-system';
import * as ROUTES from './routes';

export default props => {
  return (
  <Row style={{backgroundColor: "black", width: "100%",color: "white", padding: "10px", margin: "5px"}}>
        <Col sm={2}>
          Your Company
        </Col>
        <Col sm={9}>
          Search
        </Col>
        <Col sm={1}>
        <a className="menu-item" id="dashboard" href={ROUTES.getRoutePath(ROUTES.LOGTIME)} style={{textDecoration: "none", color:"white"}}> Log Time </a>
        </Col>
      </Row>
        );
    };