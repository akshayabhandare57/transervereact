import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import * as ROUTES from './routes';

import MainPage from './Main';
import Employees from './Employees';
import Projects from './Projects';
import LogTime from './LogTime';

const NoMatch = () => <Redirect to={ROUTES.MAIN} />
const RootApp = (    
    <Switch>
        <Route exact path={ROUTES.MAIN} component={MainPage}></Route>
        <Route exact path={ROUTES.EMPLOYEES} component={Employees}></Route>
        <Route exact path={ROUTES.PROJECTS} component={Projects}></Route>
        <Route exact path={ROUTES.LOGTIME} component={LogTime}></Route>
        <Route component={NoMatch} />
    </Switch>                
)
export default RootApp;
