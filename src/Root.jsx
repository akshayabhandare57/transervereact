import React, {Component} from 'react';
import {HashRouter as Router} from 'react-router-dom'
import RootApp from './RootApp'
import axios from 'axios'

class Root extends Component {
  render() {
    return (
        <Router>
        <div>
            {RootApp}
        </div>
        </Router>           
    );
  }
}

export default Root;
