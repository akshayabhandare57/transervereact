import React, { Component } from "react";
import { Container, Row, Col } from 'react-grid-system';
import SideBar from './Sidebar';
import Header from './Header';
import './Sidebar.css';
 
class Projects extends Component {
  render() {
    return (
      <div>
        {/* Header */}
      <Header />
      {/* Sidebar */}
      <Row style={{backgroundColor: "white", color: "black", padding: "10px", margin: "5px"}}>
        <Col sm={1}>
          <SideBar />
        </Col>
        <Col sm={11}>
           <h1>Projects</h1>
        </Col>
      </Row>
    </div>
    );
  }
}
 
export default Projects;