import React, { Component } from "react";
import { Container, Row, Col } from 'react-grid-system';
import SideBar from './Sidebar';
import Header from './Header';
import './Sidebar.css';
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import axios from 'axios';
import { LOGTIME } from "./api";
 
class Logtime extends Component {
    constructor(props) 
    {
        super(props);
        const { data = {} } = props;    
        this.state = {
          date: this.getDate(),
          time:'',
          employee: '',
          projects:''
        };
    }
    getDate() 
    {
        let nowDate = new Date();
        var d = nowDate.getDate();
        var m = nowDate.getMonth() + 1;
        var y = nowDate.getFullYear();
        return "" + y + "-" + (m <= 9 ? "0" + m : m) + "-" + (d <= 9 ? "0" + d : d);
    }
    handleInputChange = name => event => {
        event.persist();
        let value = event.target.value;
        if (name == "date") 
        {
            this.setState({ date: value});
          //this.state.date = value;    
        } 
        else if (name == "time") 
        {
            this.setState({ time: value});
          this.state.time = value;
        }
        else if (name == "projects") 
        {
            this.setState({ projects: value});
          this.state.time = value;
        }
        else if (name == "employee") 
        {
            this.setState({ employee: value});
          this.state.time = value;
        }
      }
    onSubmitHandler = () => {
        console.log("akshaya", this.state);
        axios.post(LOGTIME, this.state)
        .then(({data}) => {
        })
        .catch(
            error => {if(error.response){
            error = error.response;
        }}
        );
        return false;
    }

render() 
{
return (
    <div>
    {/* Header */}
    <Header />
    {/* Sidebar */}
    <Row style={{backgroundColor: "white", color: "black", padding: "10px", margin: "5px"}}>
    <Col sm={1}>
        <SideBar />
    </Col>
    <Col sm={11}>
        <h1>Log Time Here</h1>
        <Row>
            <Col sm={6}  style={{padding: "10px", margin: "10px"}}>
            <FormControl fullWidth variant="outlined">
                <TextField
                id="outlined-name"
                label="Date:"
                margin="dense"
                variant="outlined"
                type="date"
                inputProps={{
                    max: new Date().toISOString().split("T")[0],
                    value: this.state.date
                }}
                InputLabelProps={{ shrink: true }}
                onChange={this.handleInputChange("date")}
                />
            </FormControl>
            </Col>
            <Col sm={6}></Col>
            <Col sm={6} style={{padding: "10px", margin: "10px"}}>
            <FormControl fullWidth variant="outlined">
                <TextField
                id="outlined-name"
                label="Time:"
                margin="dense"
                variant="outlined"
                value={this.state.time}
                InputLabelProps={{ shrink: true }}
                onChange={this.handleInputChange("time")}
                />
            </FormControl>
            </Col>
            <Col sm={6}></Col>
            <Col sm={6} style={{padding: "10px", margin: "10px"}}>
            <FormControl
                fullWidth
                variant="outlined"
            >
                <InputLabel
                ref={ref => {
                    this.InputLabelRef = ref;
                }}
                required={true}
                htmlFor="Projects"
                >
                Select Projects:
                </InputLabel>
                <Select
                value={this.state.projects}
                onChange={this.handleInputChange("projects")}
                input={
                    <OutlinedInput
                    name="projects"
                    labelWidth={80}
                    labelHeight={10}
                    id="projects"
                    />
                }
                >
                {[
                    { text: "Project 1", value: "1" },
                    { text: "Project 2", value: "2" }
                ].map((projects, idx) => (
                    <MenuItem key={idx} value={projects.value}>
                    {projects.text}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
            </Col>
            <Col sm={6}></Col>
            <Col sm={6} style={{padding: "10px", margin: "10px"}}>
            <FormControl
                fullWidth
                variant="outlined"
            >
                <InputLabel
                ref={ref => {
                    this.InputLabelRef = ref;
                }}
                required={true}
                htmlFor="Projects"
                >
                Select Employee:
                </InputLabel>
                <Select
                value={this.state.employee}
                onChange={this.handleInputChange("employee")}
                input={
                    <OutlinedInput
                    name="employee"
                    labelWidth={80}
                    labelHeight={10}
                    id="employee"
                    />
                }
                >
                {[
                    { text: "Employee 1", value: "1" },
                    { text: "Employee 2", value: "2" }
                ].map((employee, idx) => (
                    <MenuItem key={idx} value={employee.value}>
                    {employee.text}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
            </Col>
            <Col sm={6}>
            <Button
                variant="contained"
                color="primary"
                onClick={this.onSubmitHandler}
            >
                Submit
            </Button>
            </Col>
        </Row>
    </Col>
    </Row>
</div>
);
}
}
 
export default Logtime;